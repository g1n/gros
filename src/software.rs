use crate::{println, print};
use alloc::string::String;
use alloc::vec::Vec;
use crate::alloc::string::ToString;

use crate::software::calc::calc;

pub mod calc;

pub fn command_func(input_command: String) {
   let mut command = String::new();
   let mut args_vec: Vec<&str> = input_command.split(" ").collect();
   command = args_vec[0].to_string();
   args_vec.remove(0);
   
   match command.as_str() {
   	 "hi" => {
	 		  println!("hello command!!!");
			  }
	 "wow" => {
			  println!("wow!!");
			  }
	"echo" => {
   		  let mut args = String::new();
	       	  for i in args_vec {
   		      for j in i.chars() {
		          args.push(j);
	  	      }
       		      args.push(' ');
   		  }
	       	  println!("{} ", args);
		  }
	"asciiart" => {
		   println!("   _____ _____   ____   _____ ");
		   println!("  / ____|  __ \\ / __ \\ / ____|");
		   println!(" | |  __| |__) | |  | | (___  ");
		   println!(" | | |_ |  _  /| |  | |\\___ \\ ");
		   println!(" | |__| | | \\ \\| |__| |____) |");
		   println!("  \\_____|_|  \\_\\\\____/|_____/ ");
	    }
	"calc" => {
	       calc(args_vec);
	}
	command => {
		println!("{}: command not found", command);
		}
	}
}