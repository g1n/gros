#![no_std]
#![no_main]
#![feature(custom_test_frameworks)]
#![test_runner(gros::test_runner)]
#![reexport_test_harness_main = "test_main"]


use core::panic::PanicInfo;
use gros::{println, print};
use gros::{serial_println, serial_print};
use bootloader::{BootInfo, entry_point};
use gros::memory::BootInfoFrameAllocator;
use gros::task::{Task, simple_executor::SimpleExecutor};
use gros::task::keyboard;
use gros::task::executor::Executor; 

extern crate alloc;

use alloc::{boxed::Box, vec, vec::Vec, rc::Rc};

entry_point!(kernel_main);

fn kernel_main(boot_info: &'static BootInfo) -> ! {
    use gros::allocator;
    use gros::memory;
    use x86_64::{structures::paging::Page, VirtAddr};
    println!("Hi from GROS{}", "!");
    println!("{}", "   __ _ _ __ ___  ___");
    println!("{}", "  / _` | '__/ _ \\/ __|");
    println!("{}", " | (_| | | | (_) \\__ \\");
    println!("{}", "  \\__, |_|  \\___/|___/");
    println!("{}", "  |___/");
    println!();
    print!("> ");

    gros::init();

    let phys_mem_offset = VirtAddr::new(boot_info.physical_memory_offset);
    let mut mapper = unsafe { memory::init(phys_mem_offset) };
    let mut frame_allocator = unsafe {
        BootInfoFrameAllocator::init(&boot_info.memory_map)
    };

    allocator::init_heap(&mut mapper, &mut frame_allocator)
        .expect("heap initialization failed");

    let mut executor = Executor::new();
    executor.spawn(Task::new(keyboard::process_keyboard_input()));

    executor.run();

    #[cfg(test)]
    test_main();

    println!("It did not crash!");
    gros::hlt_loop();
}

/// This function is called on panic.
#[cfg(not(test))]
#[panic_handler]
fn panic(info: &PanicInfo) -> ! {
    println!("{}", info);
    gros::hlt_loop(); 
}

#[cfg(test)]
#[panic_handler]
fn panic(info: &PanicInfo) -> ! {
    gros::test_panic_handler(info)
}